﻿using System;

namespace _26._2
{
    class ElektrischToestel
    {
        //private string _afbeelding;
        //private string _merk;
        //private bool _power;
        //private string _type;

        public string Afbeelding { get; set; }
        public string Merk { get; set; }
        public bool Power { get; set; }

        public string Type { get; set; }

        public ElektrischToestel(string merk, string type, string afbeelding)
        {
            this.Afbeelding = afbeelding;
            this.Merk = merk;
            this.Type = type;
            this.Power = false;
        }

        public override string ToString()
        {
            return "Merk: " + Merk + Environment.NewLine + "Type: " + Type + Environment.NewLine + Power.ToString() + Environment.NewLine;
        }
    }
}
