﻿using System;

namespace _26._2
{
    class Warmwaterkoker : ElektrischToestel
    {

        public double Inhoud { get; set; }

        public Warmwaterkoker(string merk, string type, string afbeelding, double inhoud) : base(merk, type, afbeelding)
        {
            this.Inhoud = inhoud;
        }
        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + Inhoud.ToString() + " ";
        }
    }
}
