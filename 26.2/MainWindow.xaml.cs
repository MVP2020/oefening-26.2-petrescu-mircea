﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace _26._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        BindingList<Warmwaterkoker> lijstKokers = new BindingList<Warmwaterkoker>();

        TV tv = new TV("Sony", "KD-77AG9", 144, 77, @"C:\Users\MVP\source\repos\26.2\26.2\Images\Sony KD-77AG9.jpg");
        TV tv2 = new TV("Samsung", "QLED 8K 85Q950TS (2020)", 100, 80, @"C:\Users\MVP\source\repos\25.1\25.1\Images\Samsung QLED 8K 85Q950TS (2020).jpg");

        public MainWindow()
        {
            InitializeComponent();
        }

        private void rdSony_Checked(object sender, RoutedEventArgs e)
        {

            TV tv = new TV("Sony", "KD-77AG9", 144, 77, @"C:\Users\MVP\source\repos\26.2\26.2\Images\Sony KD-77AG9.jpg");
            picture.Source = new BitmapImage(new Uri(tv.Afbeelding));
            Tvdetails.Text = tv.ToString();

        }
        private void rdSamsung_Checked(object sender, RoutedEventArgs e)
        {

            TV tv2 = new TV("Samsung", "QLED 8K 85Q950TS (2020)", 100, 80, @"C:\Users\MVP\source\repos\25.1\25.1\Images\Samsung QLED 8K 85Q950TS (2020).jpg");
            picture.Source = new BitmapImage(new Uri(tv2.Afbeelding));
            Tvdetails.Text = tv2.ToString();

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (rdSony.IsChecked == true)
            {
                txtVolume.Text = tv.Volume.ToString();
                txtVolume.Text = txtVolume.Text;
                Tvdetails.Text = tv.ToString();
                txtKanaal.Text = tv.Kanaal.ToString();
                txtKanaal.Text = txtKanaal.Text;
                Tvdetails.Text = tv.ToString();

            }
            else if (rdSamsung.IsChecked == true)
            {
                txtKanaal.Text = tv2.Kanaal.ToString();
                txtVolume.Text = tv2.Volume.ToString();
            }
            else
            {
                MessageBox.Show("Please choose a TV first!");
            }


        }
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            txtKanaal.Text = "";
            txtVolume.Text = "";
        }

        public void ToonWaterkokerInfo()
        {
            //Waterkokersdetails.Text = 
        }
        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {


        }

        private void txtKanaal_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                int kanaal = Convert.ToInt32(txtKanaal.Text);
                if (kanaal > 1000)
                {
                    MessageBox.Show("You reached the maximum channel number on this TV!");
                    txtKanaal.Text = "1000";
                }
                if (kanaal < 0)
                {
                    MessageBox.Show("You reached the minimum channel!");
                    txtKanaal.Text = "0";
                }
                tv.Kanaal = kanaal;

            }
            catch
            {
                MessageBox.Show("Type a channel between 0 and 1000!");
                txtKanaal.Text = tv.Kanaal.ToString();
            }
        }

        private void CheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            Warmwaterkoker warmwaterkoker = new Warmwaterkoker("Tefal", "Performa 2 KI110D Express+ 1,7L RVS", "Tefal - Performa 2 KI110D Express+ 1,7L RVS.jpg", 1.7);
            Warmwaterkoker warmwaterkoker2 = new Warmwaterkoker("Philips", "Philips HD9357-10", @"C:\Users\MVP\source\repos\25.1\25.1\Images\Philips HD9357-10.jpg", 1.8);


            if (cmbWaterkokers.SelectedItem == "Tefal - Performa 2 KI110D Express + 1, 7L RVS")
            {
                txtWaterkokerdetails.Text = warmwaterkoker.ToString();
                image2.Source = new BitmapImage(new Uri(warmwaterkoker.Afbeelding));
            }
            else
            {
                txtWaterkokerdetails.Text = warmwaterkoker2.ToString();
                image2.Source = new BitmapImage(new Uri(warmwaterkoker2.Afbeelding));
            }
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Tefal - Performa 2 KI110D Express + 1, 7L RVS");
            data.Add("Philips HD9357 - 10");
            var combo = sender as ComboBox;
            combo.ItemsSource = data;
            combo.SelectedIndex = 0;
        }

        private void txtVolume_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int volume = Convert.ToInt32(txtVolume.Text);
                if (volume > 100)
                {
                    MessageBox.Show("You reached the maximum volume!");
                    txtVolume.Text = "100";
                }
                if (volume < 0)
                {
                    MessageBox.Show("You reached the minimum volume!");
                    txtVolume.Text = "0";
                }
                tv.Volume = volume;

            }
            catch
            {
                MessageBox.Show("Type a volume between 0 and 100!");
                txtVolume.Text = tv.Volume.ToString();
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Warmwaterkoker warmwaterkoker = new Warmwaterkoker("Tefal", "Performa 2 KI110D Express+ 1,7L RVS", "Tefal - Performa 2 KI110D Express + 1, 7L RVS.jpg", 1.7);
            Warmwaterkoker warmwaterkoker2 = new Warmwaterkoker("Philips", "Philips HD9357-10", @"C:\Users\MVP\source\repos\25.1\25.1\Images\Philips HD9357 - 10.jpg", 1.8);




            lijstKokers.Add(warmwaterkoker);
            lijstKokers.Add(warmwaterkoker2);

            cmbWaterkokers.ItemsSource = lijstKokers;
            cmbWaterkokers.DisplayMemberPath = "Type";
            cmbWaterkokers.SelectedIndex = 0;

        }

        private void cmbWaterkokers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbWaterkokers.SelectedItem != null)
            {
                Warmwaterkoker mijnWarmWaterKoker = (Warmwaterkoker)cmbWaterkokers.SelectedItem;
                txtWaterkokerdetails.Text = mijnWarmWaterKoker.ToString();
                BitmapImage pic = new BitmapImage(new Uri(@"Images\" + mijnWarmWaterKoker.Afbeelding, UriKind.Relative));
                image2.Source = pic;
                cbPower.IsChecked = mijnWarmWaterKoker.Power;
            }
        }

        private void Tvdetails_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
