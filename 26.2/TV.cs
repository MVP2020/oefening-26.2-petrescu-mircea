﻿using System;

namespace _26._2
{
    class TV : ElektrischToestel
    {
        private int _volume;
        private int _herz;
        private int _kanaal;
        private bool _teletekst;
        //private int _volume;

        public int Beeldgrootte { get; set; }

        public int Herz { get; set; }

        public int Kanaal { get; set; }

        public bool Teletekst { get; set; }

        public int Volume { get; set; }

        public TV(string merk, string type, int herz, int beeldgrootte, string afbeelding) : base(merk, type, afbeelding)
        {
            Beeldgrootte = beeldgrootte;
            Herz = herz;
            Afbeelding = afbeelding;
            Kanaal = 1;
            Volume = 0;
            Power = false;

        }

        public override string ToString()
        {
            return base.ToString() + "Herz: " + Herz.ToString() + Environment.NewLine + "Beeldgrootte: " + Beeldgrootte.ToString() + Environment.NewLine + "Kanaal:" + Kanaal.ToString() + Environment.NewLine + "Volume:" + Volume.ToString();
        }
    }
}
